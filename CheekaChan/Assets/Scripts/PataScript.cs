﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PataScript : MonoBehaviour {

	Animator anim;
	int angryWalkHash = Animator.StringToHash("AngryWalk");
	int walkHash = Animator.StringToHash("Walk");

	void Awake() {

		anim = GetComponent<Animator>();
	
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			angryWalk ();
		}
	}

	public void walk() {

		anim.SetTrigger (walkHash);

	}

	public void angryWalk() {

		anim.SetTrigger (angryWalkHash);

	}

}
