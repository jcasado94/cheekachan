﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIUtils : MonoBehaviour {

	public Texture flash;

	//flags
	private float drawFlashForSeconds;
	private bool onGUIRun;

	// Use this for initialization
	void Start () {

		drawFlashForSeconds = 0;
		onGUIRun = false;

	}

	void OnGUI() {

		if (!onGUIRun)
			drawFlashForSeconds -= Time.deltaTime;
		
		if (drawFlashForSeconds > 0)
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), flash, ScaleMode.StretchToFill);


		// at the end
		onGUIRun = true;

	}

	public void playFlashForSeconds(float seconds) {
		drawFlashForSeconds = seconds;
	}
	
	// Update is called once per frame
	void Update () {

		onGUIRun = false;

	}
}
